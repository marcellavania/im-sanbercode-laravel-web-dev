<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register', [AuthController::class, 'daftar']);
Route::post('/send', [AuthController::class, 'kirim']);

Route::get('/table', function(){
    return view ('table');
});
Route::get('/data-table', function(){
    return view ('data-table');
});

//CRUD

//Create Data
// Route mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan inputan ke dalam database tabel cast
Route::post('/cast', [CastController::class, 'store']);

//Read data
//Route mengarah ke halaman tampil semua data di tabel cast
Route::get('/cast', [CastController::class, 'index']);
//Route detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update data
//Route mengarah ke form edit cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Route untuk edit data berdasarkan id cast
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);