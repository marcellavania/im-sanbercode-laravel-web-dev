<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('form');
    }
    public function kirim(request $request){
        //dd($request);
        //dd($request->all());
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('home', ['namaDepan'=> $namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
