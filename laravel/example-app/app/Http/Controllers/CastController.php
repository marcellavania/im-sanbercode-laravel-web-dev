<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function create()
    {
        return view('cast.create');
    }
    public function store(request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => "Nama tidak boleh kosong!",
            'umur.required' => "Umur harus diisi!",
            'bio.required' => "Silahkan isikan bio!"
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }
    
    public function index(){
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.detail', ['cast'=> $cast]);
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.edit', ['cast'=> $cast]);
    }

    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => "Nama tidak boleh kosong!",
            'umur.required' => "Umur harus diisi!",
            'bio.required' => "Silahkan isikan bio!"
        ]);
        DB::table('cast')
            ->where('id', $cast_id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
        );

        return redirect('/cast');
    }

    public function destroy($cast_id){
        DB::table('cast')->where('id', '=', $cast_id)->delete();
        return redirect('/cast');
    }
}

