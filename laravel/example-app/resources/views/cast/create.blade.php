@extends('layouts.master')
@section('title')
    Halaman Tambah Cast
@endSection
@section('sub-title')
    Cast
@endSection
@section('content')
<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Cast Name</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Cast Age</label>
    <input type="text" name="umur" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <!-- <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">
  </div> -->
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endSection