@extends('layouts.master')
@section('title')
    Halaman Detail Cast
@endSection
@section('sub-title')
    Cast
@endSection
@section('content')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endSection