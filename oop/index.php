<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");
echo "Name: "  . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>";// 4
echo "Cold Blooded: " . $sheep->cold_blooded . "<br> <br>" ; // "no"

$kodok = new frog("Buduk");
echo "Name: "  . $kodok->name . "<br>"; // "shaun"
echo "Legs: " . $kodok->legs . "<br>";// 4
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
echo $kodok->jump();

$sungokong = new ape("Kera Sakti");
echo "Name: "  . $sungokong->name . "<br>"; // "shaun"
echo "Legs: " . $sungokong->legs . "<br>";// 4
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>" ; // "no"
echo $sungokong->yell();
?>